QT += core network
QT -= gui

CONFIG += c++11

TARGET = ExServer
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    server.cpp \
    client.cpp

HEADERS += \
    server.h \
    client.h \
    vec3.h
