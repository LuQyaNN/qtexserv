#include "server.h"
#include "client.h"
#include <QMutex>
#include <iostream>



Server::Server(QObject *parent) : QObject(parent)
{
    udp_server.bind(QHostAddress::LocalHost, 80);
    connect(&udp_server,SIGNAL(readyRead()),this,SLOT(OnUdpMessageRecieve()));


    connect(&tcp_server,SIGNAL(newConnection()),this,SLOT(OnConnect()));
    if(!tcp_server.listen(QHostAddress::Any,80)){
        std::cout << "tcp_server listen error" <<std::endl;
    }else{
        std::cout << "tcp server listen" <<std::endl;
    }
}

void Server::OnConnect()
{
    QTcpSocket* tcp_s = tcp_server.nextPendingConnection();
  // QThread* client_thread = new QThread;


   Client* cl = new Client(this,tcp_s);
   //connect(cl,SIGNAL(destroyed(QObject*)),client_thread,SLOT(deleteLater()));

    //cl->moveToThread(client_thread);

//client_thread->start();
}

void Server::OnUdpMessageRecieve()
{
    QByteArray buf(udp_server.pendingDatagramSize(),0);
    udp_server.readDatagram(buf.data(),udp_server.pendingDatagramSize());
    switch (buf[0]) {
            case 2: {
                QByteArray tmp_id;
                tmp_id.reserve(16);

                for(int i =0;i<16;++i){
                  tmp_id[i]=buf[i+1];
                }

                auto iter =clients_id_map.find(tmp_id);
                if(iter != clients_id_map.end()){
                    char* tbf = buf.data() + 17;
                    iter.value()->setRepInfo(tbf);
}

                break;
            }
            }
}
