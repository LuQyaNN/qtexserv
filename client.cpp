#include "client.h"
#include "server.h"
#include <QString>
#include <QCryptographicHash>
#include <iostream>
#include <QHash>
#include <QThread>
#include <QEventLoop>
#include <QMutex>


QMutex mut(QMutex::Recursive);



float uint8Tofloat(uint8_t* bytes) {
    return *reinterpret_cast<float*>(bytes);
}

float uint8Tofloat(char* bytes) {
    return *reinterpret_cast<float*>(bytes);
}

char* floatTochar(float* fl) {
    return reinterpret_cast<char*>(fl);
}

float Dist(const Location& l1, const Location& l2) {
    return sqrt((l2.x - l1.x)*(l2.x - l1.x) + (l2.y - l1.y)*(l2.y - l1.y) + (l2.z - l1.z)*(l2.z - l1.z));
}

Client::Client(Server* _server, QTcpSocket* tcp_s, QObject *parent ) :
    QObject(parent),
tcp_socket(tcp_s),
udp_socket(new QUdpSocket(this)),
server(_server),
  send_clients_info_timer(this)
{
    tcp_s->setParent(this);
    connect(tcp_s,SIGNAL(readyRead()),this,SLOT(Tcp_recieve()));
    connect(tcp_s,SIGNAL(disconnected()),this,SLOT(Tcp_disconnect()));
    connect(tcp_s,SIGNAL(disconnected()),&send_clients_info_timer,SLOT(stop()));

    std::cout << "Client connected" << std::endl;


    send_clients_info_timer.setSingleShot(false);
    send_clients_info_timer.setInterval(300);
    send_clients_info_timer.setTimerType(Qt::PreciseTimer);
std::cout << thread()->currentThreadId()<< std::endl;
//connect(thread(),SIGNAL(finished()),&send_clients_info_timer,SLOT(stop()));

    connect(&send_clients_info_timer,SIGNAL(timeout()),this,SLOT(send_clients_info()));
send_clients_info_timer.start();
}

Client::~Client()
{
   // send_clients_info_thread->join();

//mut.lock();
            server->getnmap()->remove(Nick);
            server->getidmap()->remove(id);
//mut.unlock();
//tcp_socket->deleteLater();
            delete tcp_socket;
            delete udp_socket;
//udp_socket->deleteLater();
//thread()->exit();
            std::cout << "Client deleted" << std::endl;



}

void Client::setRepInfo(char *&buf)
{
    if (!nick_recieved)return;
            Velocity		tmp_vel = { uint8Tofloat(buf) ,uint8Tofloat(buf + 4),uint8Tofloat(buf + 8) };
            AngularVelocity tmp_avel = { uint8Tofloat(buf + 12) ,uint8Tofloat(buf + 16),uint8Tofloat(buf + 20) };
            Rotation		tmp_rot = { uint8Tofloat(buf + 24) ,uint8Tofloat(buf + 28),uint8Tofloat(buf + 32) };
            Location		tmp_loc = { uint8Tofloat(buf + 36) ,uint8Tofloat(buf + 40),uint8Tofloat(buf + 44) };
            vel = tmp_vel;
            avel = tmp_avel;
            rot = tmp_rot;
            loc = tmp_loc;
}



void Client::Tcp_recieve()
{
    std::cout << thread()->currentThreadId()<< std::endl;
    QByteArray buf = tcp_socket->readAll();
    switch (buf[0]) {
            case 1: {//take nick
                OnRecieveNick(buf);
                break;
            }
            case 3://take chat message
            {
                OnRecieveChatMsg(buf);
               break;
            }
    }
}

void Client::Tcp_disconnect()
{

    deleteLater();

}

bool Client::IsValidNick(QString& nick) {
        return (server->getnmap()->end() == server->getnmap()->find(nick));
    }

void Client::OnRecieveNick(QByteArray& buf) {
        if(nick_recieved)return;
        QString tmpnick;
        tmpnick.reserve(buf.length()-1);
        for(int i =1;i<buf.length();++i){
          tmpnick[i-1]=buf[i];
        }


        if (IsValidNick(tmpnick)) {
            Nick = tmpnick;
            nick_recieved = true;

            tmpnick.append("ballserver134");

            id = QCryptographicHash::hash(tmpnick.toLocal8Bit(),QCryptographicHash::Algorithm::Md5);


           server->getnmap()->insert(Nick, this);
           server->getidmap()->insert(id, this);
           QByteArray buf1(17,'0');

            buf1[0] = 1;
            buf1.replace(1,16,id);
            tcp_socket->write(buf1);

        } else {
            QByteArray buf1(2,'0');
            buf1[0] = 0;
            buf1[1] = 0;
            tcp_socket->write(buf1);
            tcp_socket->close();
        }

        std::cout << Nick.toStdString() << std::endl;

}

void Client::OnRecieveChatMsg(QByteArray &buf)
{
    if(!nick_recieved)return;
    QByteArray buf1(1+buf.size()+Nick.size(),0);

            buf1[0]=3;
            buf1[1] = Nick.size()-1;
            for (int i = 0; i < Nick.size()-1; ++i) {
                buf1[i+2]= Nick[i].toLatin1();
            }
            for (int i = 1; i < buf.size(); ++i) {
                buf1[1+Nick.size()-1 +i]=buf[i];
            }
            for (Client* client : *server->getidmap()) {
                client->tcp_socket->write(buf1);
            }
}

void add_uint8_t_to_buf( char*& current_pos, char* float_ptr) {
        for (int i = 0; i < 4; ++i) {
            *current_pos = float_ptr[i];
            current_pos += 1;
        }
}

void fill_buffer(char*& current_pos, Velocity* v, AngularVelocity* a,
        Rotation* r, Location* l) {
        char* vel_x = floatTochar(&v->x);
        char* vel_y = floatTochar(&v->y);
        char* vel_z = floatTochar(&v->z);
        char* avel_x = floatTochar(&a->x);
        char* avel_y = floatTochar(&a->y);
        char* avel_z = floatTochar(&a->z);
        char* rot_yaw = floatTochar(&r->yaw);
        char* rot_pitch = floatTochar(&r->pitch);
        char* rot_roll = floatTochar(&r->roll);
        char* loc_x = floatTochar(&l->x);
        char* loc_y = floatTochar(&l->y);
        char* loc_z = floatTochar(&l->z);

        add_uint8_t_to_buf( current_pos,vel_x);
        add_uint8_t_to_buf( current_pos, vel_y);
        add_uint8_t_to_buf( current_pos, vel_z);
        add_uint8_t_to_buf( current_pos, avel_x);
        add_uint8_t_to_buf( current_pos, avel_y);
        add_uint8_t_to_buf( current_pos, avel_z);
        add_uint8_t_to_buf( current_pos, rot_yaw);
        add_uint8_t_to_buf( current_pos, rot_pitch);
        add_uint8_t_to_buf( current_pos, rot_roll);
        add_uint8_t_to_buf( current_pos, loc_x);
        add_uint8_t_to_buf( current_pos, loc_y);
        add_uint8_t_to_buf( current_pos, loc_z);



    }

void Client::send_clients_info()
{
    if(!nick_recieved)return;
    QHash<QString, RepInfo*> visible_clients;
                QVector<RepInfo> ClientsRepInfo;
               // mut.lock();
                for (auto client : *server->getidmap()) {

                    ClientsRepInfo.push_back(client->GetRepInfo());
                }
              //  mut.unlock();
                //mut.lock();
                for (auto& client : ClientsRepInfo) {


                    if (client.id != id) {
                        if (Dist(loc, client.loc) < 10000) {
                            if (visible_clients.find(client.id) == visible_clients.end()) {
                                visible_clients.insert(client.id, &client);
                            }
                        } else {
                            if (visible_clients.find(client.id) != visible_clients.end()) {
                                visible_clients.remove(client.id);
                            }
                        }

                    }

                }
                //mut.unlock();
                size_t buf_size=0;
                //mut.lock();
                for (auto& client : visible_clients) {

                    buf_size+=(client->Nick.size() + 66);
                }
                //mut.unlock();
                if(buf_size==0)return;
                buf_size+=2;
                QByteArray buf(buf_size,0);

                buf[0] = 2;
                buf[1] = visible_clients.size();
                char* current_pos = buf.data()+2 ;
                //mut.lock();

                for (auto& client : visible_clients) {

                    char nick_size = static_cast<char>(client->Nick.size()-1);
                    *current_pos = nick_size;
                    current_pos+=1;



                    for (char i = 0; i <nick_size; ++i) {
                        *current_pos = client->Nick[i].toLatin1();
                        current_pos+=1;
                    }
                    for (int i = 0; i < 16; ++i) {
                        *current_pos = client->id[i];
                        current_pos+=1;
                    }

                    fill_buffer(current_pos, &client->vel,
                        &client->avel,
                        &client->rot,
                        &client->loc);




                }
                //mut.unlock();

                if(udp_socket->writeDatagram(buf,tcp_socket->peerAddress(),tcp_socket->peerPort())!=-1){
                    //std::cout << "info sended" << std::endl;
                }

}








