#ifndef VEC3_H
#define VEC3_H
#include <QString>

struct vec3 {
    float x;
    float y;
    float z;
    bool operator==(const vec3& other) {
        return (x == other.x) && (y == other.y) && (z == other.z);
    }
    bool operator!=(const vec3& other) {
        return (x != other.x) && (y != other.y) && (z != other.z);
    }
};

struct  Rotation {
    float yaw;
    float pitch;
    float roll;
    bool operator==(const Rotation& other) {
        return (yaw == other.yaw) && (pitch == other.pitch) && (roll == other.roll);
    }
    bool operator!=(const Rotation& other) {
        return (yaw != other.yaw) && (pitch != other.pitch) && (roll != other.roll);
    }
};



typedef vec3 Velocity;
typedef vec3 AngularVelocity;
typedef vec3 Location;

struct RepInfo {

    Velocity vel;
    AngularVelocity avel;
    Rotation rot;
    Location loc;
    QString Nick;
   QByteArray id;
};

#endif // VEC3_H
