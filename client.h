#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <QTcpSocket>
#include <QUdpSocket>
#include "vec3.h"
#include <QTimer>
#include <QThread>

class Client : public QObject
{
    Q_OBJECT
public:
    explicit Client(class Server* _server, QTcpSocket* tcp_s, QObject *parent = 0);
~Client();

    RepInfo GetRepInfo()const{
            RepInfo r ={vel,avel,rot,loc,Nick,id};
            return r;
        }
 void setRepInfo(char*& buf);
signals:

private slots:
   void Tcp_recieve();
   void Tcp_disconnect();
    void send_clients_info();

private:

   QByteArray id;
   QTcpSocket* tcp_socket;
   QUdpSocket* udp_socket;
    class Server* server;
   bool nick_recieved = false;
   QString Nick;
   Velocity vel;
    AngularVelocity avel;
     Rotation rot;
     Location loc;

  QTimer send_clients_info_timer;


private:
    bool IsValidNick(QString& nick);
   void OnRecieveNick(QByteArray& buf);
  void  OnRecieveChatMsg(QByteArray& buf);

};

#endif // CLIENT_H
