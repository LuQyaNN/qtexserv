#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QTcpServer>
#include <QUdpSocket>



class Server : public QObject
{
    Q_OBJECT
public:
    explicit Server(QObject *parent = 0);
     QHash<QString,class Client*>* getnmap(){return &clients_nick_map;}
     QHash<QByteArray,class Client*>* getidmap(){return &clients_id_map;}
signals:

private slots:
    void OnConnect();
    void OnUdpMessageRecieve();
private:
    QHash<QString,  Client*> clients_nick_map;
    QHash<QByteArray,  Client*> clients_id_map;
    QTcpServer tcp_server;
    QUdpSocket udp_server;
};

#endif // SERVER_H
